#!/bin/sh

if [ -z "$1" ]; then
  path="/repo"
else
  path="$1"
fi

printf "populatepackages.sh\nretrieving package files from %s\n" "$path"

repos="system world galaxy lib32 system-gremlins world-gremlins galaxy-gremlins lib32-gremlins system-goblins world-goblins galaxy-goblins lib32-goblins"

for repo in $repos
do
    ./manage.py reporead x86_64 "$path/$repo/os/x86_64/$repo.db.tar.gz"

    ./manage.py reporead --filesonly x86_64 "$path/$repo/os/x86_64/$repo.files.tar.gz"

    ./manage.py readlinks "$path/$repo/os/x86_64/$repo.links.tar.gz"
done
