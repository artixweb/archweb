#!/bin/sh

if [ "$#" -ne 1 ] || [ "$1" != "-f" ]; then
    echo "Must pass -f"
    exit 1
fi

find . -type f -exec grep -Iq . {} \; -print | while read file; do
    sed -i 's/Arch Linux/Artix Linux/g' "$file"
done
